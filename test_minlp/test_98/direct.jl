using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
ENV["ARTELYS_LICENSE"]="/Users/pol/Documents/Travail/CentraleSupelec/Stages/Stage_IETR/Code/com_solvers/artelys_licenses"
using KNITRO

solver = 4
rescale = 1

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer)                                 #"display/verblevel" => 0, , "limits/gap" => 0.005
elseif solver == 4
    optimizer = optimizer_with_attributes(KNITRO.Optimizer, "ms_enable"=>1)
end

m = Model(optimizer)

#Constants:
kr = 0.7
P = 0.9
Biron = 1.5
Ech = 1e11
Gem = 10
Delp = 0.100
pcu = 0.018e-6

#Variables:
if rescale == 0
    @variable(m, 10e-3 <= D <= 500e-3, start=10e-3)
    @variable(m, 3e-3 <= la <= 50e-3, start=3e-3)
    @variable(m, 1e-3 <= E <= 50e-3, start=1e-3)
    @variable(m, 1e-3 <= C <= 50e-3, start=1e-3)
    @variable(m, 1e5 <= j <= 100e5, start=1e5)
    @variable(m, 1e-3 <= g <= 50e-3, start=1e-3)
    Echp = Ech
    Gemp = Gem
    jp = j
elseif rescale == 1
    @variable(m, 10 <= Dp <= 500, start = 10)
    D = Dp * 10^-3
    m[:D] = D
    @variable(m, 3 <= lap <= 50, start = 3)
    la = lap * 10^-3
    m[:la]=la
    @variable(m, 1 <= Ep <= 50, start = 1)
    E = Ep * 10^-3
    m[:E] = E
    @variable(m, 1 <= Cp <= 50, start = 1)
    C = Cp * 10^-3
    m[:C] = C
    @variable(m, 0.1 <= jp <= 10, start = 0)
    j = jp * 10^6
    m[:j] = j
    @variable(m, 1 <= gp <= 50, start = 0)
    g = gp * 10^-3
    m[:g] = g
    Echp = Ech * 10^-12
    Gemp = Gem * 10^-6
end
@variable(m, 1 <= l <= 2.5, start = 1)
@variable(m, 0.8 <= beta <= 1, start = 0.8)
@variable(m, 0.1 <= Be <= 1, start = 0.1)
@variable(m, 0.01 <= Kf <= 0.5, start = 0.01)
@variable(m, 1 <= p <= 10, Int, start = 1)
@variable(m, objvar)

m[:Gem] = (pi/(2*l))*(1-Kf)*sqrt(kr*beta*Echp*E)D^2*(D+E)*Be
m[:Ech] = kr*E*jp^2
m[:Kft] = 1.5*p*beta*((g+E)/D)
m[:Bet] = (2*la*P)/(D*log((D+2E)/(D-2(la+g))))
m[:Ct] = ((pi*beta*Be)/(4*p*Biron))*D
m[:pt] = pi *D/Delp
m[:Va] = pi * (D/l) * (D+E-g-la) * (2*C+E+g+la)
m[:Vm] = pi * beta * la * (D/l) * (D-2*g-la)
m[:Pj] = pi * pcu * (D/l) * (D+E) * Ech

@objective(m, Min, objvar)
@NLconstraint(m, objvar >= m[:Pj])

@NLconstraint(m, m[:Gem] == Gemp)
@NLconstraint(m, m[:Ech] == Echp)
@NLconstraint(m, m[:Kft] == Kf)
@NLconstraint(m, m[:Bet] == Be)
@NLconstraint(m, m[:Ct] == C)
@NLconstraint(m, m[:pt] == p)

@time optimize!(m)