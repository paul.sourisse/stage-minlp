#Test du modèle dans le cas (I)

kr = 0.7
P = 0.9
Biron = 1.5
Ech = 1e11
Gem = 10
Dp = 0.100
pcu = 0.018e-6

D = 0.12732395447351627
l = 1.9400495420557038
la = 0.005535704016178784
E = 0.0038823296229908186
C = 0.0062226345663037195
beta = 0.8000000000715186
p = 4
Be = 0.4666975924315064
Jcu = 5.863136824165275e6
Kf = 0.18442012449468237
e= 0.0010095661130688556

Vu = pi * (D/l) * (D+E-e-la) * (2*C+E+e+la)
Va = pi * beta * la * (D/l) * (D-2*e-la)
Pj = pi * pcu * (D/l) * (D+E) * Ech

Gemt = (pi/(2*l))*(1-Kf)*sqrt(kr*beta*Ech*E)D^2*(D+E)*Be
Echt = kr*E*Jcu^2
Kft = 1.5*p*beta*((e+E)/D)
Bet = (2*la*P)/(D*log((D+2E)/(D-2(la+e))))
Ct = ((pi*beta*Be)/(4*p*Biron))*D
pt = pi*D/Dp