function def_model(optimizer,obj::Symbol=:Vm,rescale::Int=1)
    m = Model(optimizer)

    pcu = 0.018e-6
    kr = 0.7
    P = 0.9
    Biron = 1.5
    Delp = 0.1
    Ech = 10e10
    Echp = Ech * 10^-12
    Gem = 10
    Gemp = Gem * 10^-6
    
    if rescale == 0
        @variable(m, 10e-3 <= D <= 500e-3, start = 10e-3)
        @variable(m, 3e-3 <= la <= 50e-3, start = 3e-3)
        @variable(m, 1e-3 <= E <= 50e-3, start = 1e-3)
        @variable(m, 1e-3 <= C <= 50e-3, start = 1e-3)
        @variable(m, 1e5 <= j <= 100e5, start = 1e5)
        @variable(m, 1e-3 <= g <= 50e-3, start = 1e-3)
    elseif rescale == 1
        @variable(m, 10 <= Dp <= 500, start = 10)
        D = Dp * 10^-3
        m[:D] = D
        @variable(m, 3 <= lap <= 50, start = 3)
        la = lap * 10^-3
        m[:la] = la
        @variable(m, 1 <= Ep <= 50, start = 1)
        E = Ep * 10^-3
        m[:E] = E
        @variable(m, 1 <= Cp <= 50, start = 1)
        C = Cp * 10^-3
        m[:C] = C
        @variable(m, 0.1 <= jp <= 10, start = 0.1)
        j = jp * 10^6
        m[:j] = j
        @variable(m, 1 <= gp <= 50, start = 1)
        g = gp * 10^-3
        m[:g] = g
    end
    @variable(m, 1 <= l <= 2.5, start=1)
    @variable(m, 0.8 <= beta <= 1, start = 0.8)
    @variable(m, 0.1 <= Be <= 1, start = 0.1)
    @variable(m, 0.01 <= Kf <= 0.5, start = 0.01)
    @variable(m, 1 <= p <= 10, Int, start = 1)
    @variable(m, objvar)
    @objective(m, Min, objvar)

    m[:Geml] = (pi/(2*l))*(1-Kf)*sqrt(kr*beta*Echp*E)D^2*(D+E)*Be
    m[:Echl] = kr*E*jp^2
    m[:Kfl] = 1.5*p*beta*((g+E)/D)
    m[:Bel] = (2*la*P)/(D*log((D+2E)/(D-2(la+g))))
    m[:Cl] = ((pi*beta*Be)/(4*p*Biron))*D
    m[:pl] = pi *D/Delp
    m[:Va] = pi * (D/l) * (D+E-g-la) * (2*C+E+g+la)
    m[:Vm] = pi * beta * la * (D/l) * (D-2*g-la)
    m[:Pj] = pi * pcu * (D/l) * (D+E) * Ech

    @objective(m, Min, objvar)
    @NLconstraint(m, objvar >= m[obj])
    @NLconstraint(m, m[:Geml] == Gemp)
    @NLconstraint(m, m[:Echl] == Echp)
    @NLconstraint(m, m[:Kfl] == Kf)
    @NLconstraint(m, m[:Bel] == Be)
    @NLconstraint(m, m[:Cl] == C)
    @NLconstraint(m, m[:pl] == p)
    return m
end

idlist = [:D,:l,:la,:E,:C,:beta,:Be,:j,:Kf,:g,:p,:Geml,:Echl,:Kfl,:Bel,:Cl,:pl,:Va,:Vm,:Pj]
objlist = [:Va,:Vm,:Pj]