using JuMP

#constants:
kr = 0.7
beta = 0.85
g = 1e-3
q = 3
m = 1
TJ = 130
Te = 55
Tc = 50
pcu = 0.018e-6
he = 12
ha = 4
tm = 10^7
pco = 8900
pal = 2700

#Variable's value when optimizing Ma:
D = 130.2e-3
E= 7.5e-3
L=50.1e-3
j=5.1e6
a = 4.0e-3
d = 4.0e-3
la = 4.2e-3
C = 5.6e-3
p = 8
se = 1
sf = 1
sr = 0
sm = 1
smt = 0


#functions:
if sr == 0
    Ssr = -1
else
    Ssr = 1
end
#Ssr = 2 * sr - 1
#magnetic material:
if sm == 0
    Jsm = 0.6
    pPM = 6000
else
    Jsm = 0.9
    pPM = 7900
end
if smt == 0
    BM = 1.2
    pCM = 6000
else
    BM = 1.2
    pCM = 7900
end
#Jsm = 0.6 + 0.3 * sm                                                                                               #ok
#pPM = 6000 + 1900 * sm                                                                                             #ok
#BM = 1.3 + 0.3 * smt                                                                                               #ok
#pCM = 6000 + 1900 * smt                                                                                            #ok
#magnetic model:
Kf = 1.5 * p * beta * ((E + g) / D) *  (1 - se) * sf                                                                #Expr: checked      Value: ?
kG = (pi / 2) * ((sf) * (1 - Kf) * sqrt(beta) + (1-sf) * (sqrt(2) / 2) * sin(beta * (pi / 2)))                      #Expr: checked      Value: ?
Ne = pi * (D + Ssr * E) / (d + a)                                                                                   #Expr: checked      Value: 54.04 au lieu de 54
kc = 1 / (1 - se * (Ne * a^2 / (5 * pi * D * g + pi * D * a)))                                                      #Expr: checked      Value: ?
Be = (2 * Jsm * la / (Ssr * D * log((D + 2 * E * Ssr * (1-se)) / (D - 2 * Ssr * (la + g))))) * (1 / kc)             #Expr: checked      Value: ok
KS = kr * E * j * ((se) * (a / (a + d)) + (1-se))                                                                   #Expr: checked      Value: 11.970e3 au lieu de 12.3e3
Gem = kG * D * (D + (1-se) * Ssr * E) * L * Be * KS                                                                 #Expr: checked      Value: 9.6 au lieu de 10            
Bt = ((a + d) / d) * Be                                                                                             #Expr: checked      Value: ok
Bc = (D / (2 * p * C)) * (beta * (pi / 2) * (sf) + (1-sf)) * Be                                                     #Expr: checked      Value: 0.89 au lieu de 1.19         Ok en échangeant les sf et 1-sf
kd = se * (d / (d + a))                                                                                             #Expr: checked      Value: ok
#thermal model:
Rc = 1 / (pi * (D + 2 * Ssr * (E + C)) * L * ha)                                                                    #Expr: checked      Value: ?
Re = 1 / (pi * D * L * he)                                                                                          #Expr: checked      Value: ?
PTh = ((TJ - Te) / Re) + ((TJ - Tc) / Rc)                                                                           #Expr: checked      Value: 29.9 au lieu de 28.9
PTh2 = pcu * pi * j * L * (D + Ssr * (1-se) * E) * 12.3e3
#geometric relations:
Rint = (D / 2) - C - (1 - sr) * E - sr * (g + la)                                                                   #Expr: checked      Value: ok
Rext = (D / 2) + C + sr * E + (1 - sr) * (g + la)                                                                   #Expr: checked      Value: ok
Vm = beta * pi * L * la * (D - Ssr * (2 * g + la))                                                                  #Expr: checked      Value: 8,04 au lieu de 7,9
Vc = 2 * pi * L * C * (D + Ssr * (E - g - la))                                                                      #Expr: checked      Value: 21,8 au lieu de 21,6
Vt = pi * L * E * (D + Ssr * E) * ((1- beta) * (1-se) + (d / (d + a)) * (se))                                       #Expr: checked      Value: 2,2 au lieu de 7,4           Ok en échangeant les (1-se) et se
Vco = kr * pi * L * E * (D + Ssr * E) * (beta * (1 - se) + (a / (d + a)) * se)                                      #Expr: checked      Value: 5,17 au lieu de 5,3
Vg = (pi * L / 4) * (sr * (D + 2 * (E + C))^2 + (1 - sr) * (D + 2 * (g + la + C))^2)                                #Expr: checked      Value: ok
Ma = Vm * pPM + Vc * pCM + Vt * (pCM * se + pal * (1 - se)) + Vco * pco                                             #Expr: checked      Value: 2,53 au lieu de 2,84         Ok avec la modif de Vd