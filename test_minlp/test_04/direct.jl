using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP

solver = 4

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
elseif solver == 4
    optimizer = optimizer_with_attributes(KNITRO.Optimizer, "mip_multistart" => 1)
end

model = Model(optimizer)

if solver == 3
    set_time_limit_sec(model, 600.0)
end

#constants:
kr = 0.7
beta = 0.85
g = 1e-3
q = 3
m = 1
TJ = 130
Te = 55
Tc = 50
pcu = 0.018e-6
he = 12
ha = 4
tm = 1e7
pco = 8900
pal = 2700

#variables:
#continuous:
@variable(model, 1e-3<=D<=300e-3, start = 1e-3)
@variable(model,4e-3<=E<=100e-3, start = 4e-3)
@variable(model, 50e-3<=L<=150e-3, start = 50e-3)
@variable(model, 3e6<=j<=6e6, start = 3e6)
@variable(model, 4e-3<=a, start = 4e-3)
@variable(model, 4e-3<=d, start = 4e-3)
@variable(model, 4e-3<=la<=100e-3, start = 4e-3)
@variable(model, 4e-3<=C<=100e-3, start = 4e-3)
@variable(model, y)
#integer:
@variable(model, 1<=p<=16,Int, start = 1)
#Binary:
@variable(model, se, Bin, start = 0)
@variable(model, sf, Bin, start = 0)
@variable(model, sr, Bin, start = 0)
@variable(model, sm, Bin, start = 0)
@variable(model, smt, Bin, start = 0)

#functions:
Ssr = 2 * sr - 1
#magnetic material:
Jsm = 0.6 + 0.3 * sm                                                                                               
BM = 1.3 + 0.3 * smt                                                                                                
pPM = 6000 + 1900 * sm                                                                                              
pCM = 6000 + 1900 * smt                                                                                             
#magnetic model:
Kf = 1.5 * p * beta * ((E + g) / D) *  (1 - se) * sf                                                                #Expr: checked      Value: ?
kG = (pi / 2) * ((sf) * (1 - Kf) * sqrt(beta) + (1-sf) * (sqrt(2) / 2) * sin(beta * (pi / 2)))                      #Expr: checked      Value: ?
Ne = pi * (D + Ssr * E) / (d + a)                                                                                   #Expr: checked      Value: 54.04 au lieu de 54
kc = 1 / (1 - se * (Ne * a^2 / (5 * pi * D * g + pi * D * a)))                                                      #Expr: checked      Value: ?
Be = (2 * Jsm * la / (Ssr * D * log((D + 2 * E * Ssr * (1-se)) / (D - 2 * Ssr * (la + g))))) * (1 / kc)             #Expr: checked      Value: ok
KS = kr * E * j * ((se) * (a / (a + d)) + (1-se))                                                                   #Expr: checked      Value: 11.970e3 au lieu de 12.3e3
Gem = kG * D * (D + (1-se) * Ssr * E) * L * Be * KS                                                                 #Expr: checked      Value: 9.6 au lieu de 10            Ok en remplaçant 1-se par se
Bt = ((a + d) / d) * Be                                                                                             #Expr: checked      Value: ok
Bc = (D / (2 * p * C)) * (beta * (pi / 2) * (sf) + (1-sf)) * Be                                                     #Expr: checked      Value: 0.89 au lieu de 1.19         Ok en échangeant les sf et 1-sf
kd = se * (d / (d + a))                                                                                             #Expr: checked      Value: ok
#thermal model:
Rc = 1 / (pi * (D + 2 * Ssr * (E + C)) * L * ha)                                                                    #Expr: checked      Value: ?
Re = 1 / (pi * D * L * he)                                                                                          #Expr: checked      Value: ?
PTh = ((TJ - Te) / Re) + ((TJ - Tc) / Rc)                                                                           #Expr: checked      Value: 29.9 au lieu de 28.9
PTh2 = pcu * pi * j * L * (D + Ssr * (1-se) * E) * 12.3e3
#geometric relations:
Rint = (D / 2) - C - (1 - sr) * E - sr * (g + la)                                                                   #Expr: checked      Value: ok
Rext = (D / 2) + C + sr * E + (1 - sr) * (g + la)                                                                   #Expr: checked      Value: ok
Vm = beta * pi * L * la * (D - Ssr * (2 * g + la))                                                                  #Expr: checked      Value: 8,04 au lieu de 7,9
Vc = 2 * pi * L * C * (D + Ssr * (E - g - la))                                                                      #Expr: checked      Value: 21,8 au lieu de 21,6
Vt = pi * L * E * (D + Ssr * E) * ((1- beta) * (1-se) + (d / (d + a)) * (se))                                       #Expr: checked      Value: 2,2 au lieu de 7,4           Ok en échangeant les (1-se) et se
Vco = kr * pi * L * E * (D + Ssr * E) * (beta * (1 - se) + (a / (d + a)) * se)                                      #Expr: checked      Value: 5,17 au lieu de 5,3
Vg = (pi * L / 4) * (sr * (D + 2 * (E + C))^2 + (1 - sr) * (D + 2 * (g + la + C))^2)                                #Expr: checked      Value: ok
Ma = Vm * pPM + Vc * pCM + Vt * (pCM * se + pal * (1 - se)) + Vco * pco                                             #Expr: checked      Value: 2,53 au lieu de 2,84         Ok avec la modif de Vd

minMa = 2.7206204540353784
minVg = 0.835279420776954
fobj = Ma/minMa + Vg/minVg                                   

#cost function (Ma, Vg or fobj):
@objective(model, Min, y)

#constraints:
@NLconstraint(model, c1, Gem == 10)
@NLconstraint(model, c2a, KS >= 500)
@NLconstraint(model, c2b, KS <= 10^5)
@NLconstraint(model, c3a, Kf >= 0)
@NLconstraint(model, c3b, Kf <= 0.3)
@NLconstraint(model, c4a, Be >= 0.4)
@NLconstraint(model, c4b, Be <= 0.9)
@NLconstraint(model, c5, Ne == 2 * p * q * m)
@NLconstraint(model, c6a, Bt >= 0.89)
@NLconstraint(model, c6b, se*Bt <= BM)
@NLconstraint(model, c7a, Bc >= 0.89)
@NLconstraint(model, c7b, Bc <= BM)
@NLconstraint(model, c8a, kd >= 0.4)
@NLconstraint(model, c8b, kd <= 0.6)
@NLconstraint(model, c9, PTh == pcu * pi * j * L * (D + Ssr * (1 - se) * E) * KS)
@NLconstraint(model, c10, Rint >= 50e-3)
@NLconstraint(model, c11, Rext <= 80e-3)

@NLconstraint(model,c13, y >= Vg)


@time optimize!(model)