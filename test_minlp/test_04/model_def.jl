function def_model(optimizer,obj::Symbol=:Ma)
    model = Model(optimizer)

    kr = 0.7
    beta = 0.85
    g = 1e-3
    q = 3
    m = 1
    TJ = 130
    Te = 55
    Tc = 50
    pcu = 0.018e-6
    he = 12
    ha = 4
    dcu = 8900
    dal = 2700

    @variable(model, 1<=Dp<=300, start = 1)
    D = Dp * 10^-3
    model[:D] = D
    @variable(model,4<=Ep<=100, start = 4)
    E = Ep * 10^-3
    model[:E] = E
    @variable(model, 50<=Lp<=150, start = 50)
    L = Lp * 10^-3
    model[:L] = L
    @variable(model, 3<=jp<=6, start = 3)
    j = jp * 10^6
    model[:j] = j
    @variable(model, 4<=ap, start = 4)
    a = ap * 10^-3
    model[:a] = a
    @variable(model, 4<=dp, start = 4)
    d = dp * 10^-3
    model[:d] = d
    @variable(model, 4<=lap<=100, start = 4)
    la = lap * 10^-3
    model[:la] = la
    @variable(model, 4<=Cp<=100, start = 4)
    C = Cp * 10^-3
    model[:C] = C
    @variable(model, objvar)
    @variable(model, 1<=p<=16,Int, start = 1)
    @variable(model, se, Bin, start = 0)
    @variable(model, sf, Bin, start = 0)
    @variable(model, sr, Bin, start = 0)
    @variable(model, sm, Bin, start = 0)
    @variable(model, smt, Bin, start = 0)

    #functions:
    model[:Ssr] = 2 * sr - 1
    #magnetic material:
    model[:Jsm] = 0.6 + 0.3 * sm                                                                                               
    model[:BM] = 1.2 + 0.3 * smt                                                                                               
    model[:dPM] = 6000 + 1900 * sm                                                                                              
    model[:dCM] = 6000 + 1900 * smt                                                                                             
    #magnetic model:
    model[:Kf] = 1.5 * p * beta * ((E + g) / D) *  (1 - se) * sf                                                                #Expr: checked      Value: ?
    model[:kG] = (pi / 2) * ((sf) * (1 - model[:Kf]) * sqrt(beta) + (1-sf) * (sqrt(2) / 2) * sin(beta * (pi / 2)))                      #Expr: checked      Value: ?
    model[:Nel] = pi * (D + model[:Ssr] * E) / (d + a)                                                                                  #Expr: checked      Value: 54.04 au lieu de 54
    model[:Ner] = 2 * p * q * m
    model[:kc] = 1 / (1 - se * (model[:Nel] * a^2 / (5 * pi * D * g + pi * D * a)))                                                      #Expr: checked      Value: ?
    model[:Be] = (2 * model[:Jsm] * la / (model[:Ssr] * D * log((D + 2 * E * model[:Ssr] * (1-se)) / (D - 2 * model[:Ssr] * (la + g))))) * (1 / model[:kc])             #Expr: checked      Value: ok
    model[:KS] = kr * E * j * ((se) * (a / (a + d)) + (1-se))                                                                   #Expr: checked      Value: 11.970e3 au lieu de 12.3e3
    model[:Gem] = model[:kG] * D * (D + (1-se) * model[:Ssr] * E) * L * model[:Be] * model[:KS]                                                                 #Expr: checked      Value: 9.6 au lieu de 10            Ok en remplaçant 1-se par se
    model[:Bt] = ((a + d) / d) * model[:Be]                                                                                             #Expr: checked      Value: ok
    model[:Bc] = (D / (2 * p * C)) * (beta * (pi / 2) * (sf) + (1-sf)) * model[:Be]                                                     #Expr: checked      Value: 0.89 au lieu de 1.19         Ok en échangeant les sf et 1-sf
    model[:kd] = se * (d / (d + a))                                                                                             #Expr: checked      Value: ok
    #thermal model:
    model[:Rc] = 1 / (pi * (D + 2 * model[:Ssr] * (E + C)) * L * ha)                                                                    #Expr: checked      Value: ?
    model[:Re] = 1 / (pi * D * L * he)                                                                                          #Expr: checked      Value: ?
    model[:PThl] = ((TJ - Te) / model[:Re]) + ((TJ - Tc) / model[:Rc])                                                                           #Expr: checked      Value: 29.9 au lieu de 28.9
    model[:PThr] = pcu * pi * j * L * (D + model[:Ssr] * (1-se) * E) * model[:KS]
    #geometric relations:
    model[:Rint] = (D / 2) - C - (1 - sr) * E - sr * (g + la)                                                                   #Expr: checked      Value: ok
    model[:Rext] = (D / 2) + C + sr * E + (1 - sr) * (g + la)                                                                   #Expr: checked      Value: ok
    model[:Vm] = beta * pi * L * la * (D - model[:Ssr] * (2 * g + la))                                                                  #Expr: checked      Value: 8,04 au lieu de 7,9
    model[:Vc] = 2 * pi * L * C * (D + model[:Ssr] * (E - g - la))                                                                      #Expr: checked      Value: 21,8 au lieu de 21,6
    model[:Vt] = pi * L * E * (D + model[:Ssr] * E) * ((1- beta) * (1-se) + (d / (d + a)) * (se))                                       #Expr: checked      Value: 2,2 au lieu de 7,4           Ok en échangeant les (1-se) et se
    model[:Vco] = kr * pi * L * E * (D + model[:Ssr] * E) * (beta * (1 - se) + (a / (d + a)) * se)                                      #Expr: checked      Value: 5,17 au lieu de 5,3
    model[:Vg] = (pi * L / 4) * (sr * (D + 2 * (E + C))^2 + (1 - sr) * (D + 2 * (g + la + C))^2)                                #Expr: checked      Value: ok
    model[:Ma] = model[:Vm] * model[:dPM] + model[:Vc] * model[:dCM] + model[:Vt] * (model[:dCM] * se + dal * (1 - se)) + model[:Vco] *dcu                                             #Expr: checked      Value: 2,53 au lieu de 2,84         Ok avec la modif de Vd                             

    @objective(model, Min, objvar)

    #constraints:
    @NLconstraint(model, c1, model[:Gem] == 10)
    @NLconstraint(model, c2a, model[:KS] >= 500)
    @NLconstraint(model, c2b, model[:KS] <= 10^5)
    @NLconstraint(model, c3a, model[:Kf] >= 0)
    @NLconstraint(model, c3b, model[:Kf] <= 0.3)
    @NLconstraint(model, c4a, model[:Be] >= 0.4)
    @NLconstraint(model, c4b, model[:Be] <= 0.9)
    @NLconstraint(model, c5, model[:Nel] == model[:Ner])
    @NLconstraint(model, c6a, model[:Bt] >= 0.89)
    @NLconstraint(model, c6b, se*model[:Bt] <= model[:BM])
    @NLconstraint(model, c7a, model[:Bc] >= 0.89)
    @NLconstraint(model, c7b, model[:Bc] <= model[:BM])
    @NLconstraint(model, c8a, model[:kd] >= 0.4)
    @NLconstraint(model, c8b, model[:kd] <= 0.6)
    @NLconstraint(model, c9, model[:PThl] == model[:PThr])
    @NLconstraint(model, c10, model[:Rint] >= 50e-3)
    @NLconstraint(model, c11, model[:Rext] <= 80e-3)

    @NLconstraint(model,c13, objvar >= model[obj])

    return model
end

idlist = [:D,:E,:L,:a,:d,:la,:C,:j,:p,:se,:sf,:sr,:sm,:smt,:Be,:KS,:kd,:BM,:Bc,:Bt,:Rint,:Rext,:Gem,:Vc,:Vt,:Vco,:Vm,:Ma,:Vg,:Nel,:Ner,:PThl,:PThr]
objlist = [:Ma,:Vg]