using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
using KNITRO
import DataFrames, XLSX

solvid = 1

function get_solver(solver::Int=1)
    if solver == 1                                                                                                  #Juniper + Ipopt
        nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
        optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
        name = "Juniper"
    elseif solver == 2                                                                                              #Couenne
        optimizer = optimizer_with_attributes(KNITRO.Optimizer)
        name = "KNITRO"
    elseif solver == 3                                                                                              #SCIP
        optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
        name = "SCIP"
    elseif solver == 4
        optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
        name = "Couenne"
    end
    return optimizer,name
end

function get_variables(model::Model,idlist::Vector{Symbol},opt::Bool=true)
    dict = object_dictionary(model)
    names = []
    values = []
    for key in idlist
        var = dict[key]
        name = string(key)
        push!(names,name)
        if opt == true
            val = value(var)
            push!(values,val)
        end
    end
    return names,values
end

function gather_data(problem::Int=98,rescale::Int=1)
    #import du problème
    if problem == 98
        include("test_98/model_def.jl")
        println("ok 98")
    elseif problem == 04
        include("test_04/model_def.jl")
        println("ok 04")
    end
    #création de la colonne des noms
    par_name = ["Objective","Solver"]
    solver_init,slov_name = get_solver(1)
    model_init = def_model(solver_init)
    println(object_dictionary(model_init))
    varnames,val = get_variables(model_init,idlist,false)
    name_col = vcat(par_name,varnames)
    push!(name_col,"Time (s):")
    global results = name_col
    global n = length(name_col)
    println(results)
    for obj in objlist
        local obj_name = string(obj)
        for solvid = 1:4
            local optimizer,opt_name = get_solver(solvid)
            local par_val = [obj_name,opt_name]
            local model = def_model(optimizer,obj)
            if solvid == 3
                set_time_limit_sec(model, 600.0)
            end
            local t = @elapsed begin
                optimize!(model)
            end
            local varn,var_val = get_variables(model,idlist)
            local values = vcat(par_val,var_val)
            push!(values,t)
            local temp = hcat(results,values)
            global results = temp
        end
    end
    return results
end

results = gather_data(04)
df = DataFrames.DataFrame(results, :auto)
XLSX.writetable("data04_initupdt.xlsx", df)