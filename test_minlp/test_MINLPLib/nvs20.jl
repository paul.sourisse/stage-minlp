using MINLPLib
using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
ENV["ARTELYS_LICENSE"]="/Users/pol/Documents/Travail/CentraleSupelec/Stages/Stage_IETR/Code/com_solvers/artelys_licenses"
using KNITRO
import DataFrames, XLSX

solver = 1
auto = 0

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
elseif solver == 4
    optimizer = optimizer_with_attributes(KNITRO.Optimizer)
end

function sqr(x)
    return x^2
end

function def_opt(solvint::Int)
    if solvint == 1                                                                                     
        nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
        optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
        name = "Juniper"
    elseif solvint == 2
        optimizer = optimizer_with_attributes(KNITRO.Optimizer)
        name = "KNITRO"
    elseif solvint == 3                                                                                           
        optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)     
        name = "SCIP"                          
    elseif solvint == 4                                                                                            
        optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
        name = "Couenne"
    end
    return optimizer,name
end


m = Model()

@variable(m,0<=i[1:5],Int)
@variable(m,0<=x[6:16])
@variable(m,objvar)

e1 = 0.22*i[1] + 0.2*i[2] + 0.19*i[3] + 0.25*i[4] + 0.15*i[5] + 0.11*x[6] + 0.12*x[7]+ 0.13*x[8] + x[9]
e2 = - 1.46*i[1] - 1.3*i[3] + 1.82*i[4] - 1.15*i[5] + 0.8*x[7] + x[10]
e3 = 1.29*i[1] - 0.89*i[2] - 1.16*i[5] - 0.96*x[6] - 0.49*x[8] + x[11]
e4 = - 1.1*i[1] - 1.06*i[2] + 0.95*i[3] - 0.54*i[4] - 1.78*x[6] - 0.41*x[7] + x[12]
e5 = - 1.43*i[4] + 1.51*i[5] + 0.59*x[6] - 0.33*x[7] - 0.43*x[8] + x[13]
e6 = - 1.72*i[2] - 0.33*i[3] + 1.62*i[5] + 1.24*x[6] + 0.21*x[7] - 0.26*x[8] + x[14]
e7 = 1.12*i[1] + 0.31*i[4] + 1.12*x[7] - 0.36*x[9] + x[15]
e8 = 0.45*i[2] + 0.26*i[3] - 1.1*i[4] + 0.58*i[5] - 1.03*x[7] + 0.1*x[8] + x[16]
e9 = -((1 + i[1]^2 + i[1])^2 + (1 + i[1]^2 + i[1])*(1 + i[4]^2 + i[4]) + (1 + i[1]^2 + i[1])*(1 + x[7]^2 + x[7]) + (1 + i[1]^2 + i[1])*(1 + x[8]^2 + x[8]) + (1 + i[1]^2 + i[1])*(1 + x[16]^2 + x[16]) + (1 + i[2]^2 + i[2])^2 + (1 + i[2]^2 + i[2])*(1 + i[3]^2 + i[3]) + (1 + i[2]^2 + i[2])*(1 + x[7]^2 + x[7]) + (1 + i[2]^2 + i[2])*(1 + x[10]^2 + x[10]) + (1 + i[3]^2 + i[3]) + (1 + i[3]^2 + i[3])*(1 + x[7]^2 + x[7]) + (1 + i[3]^2 + i[3])*(1 + x[9]^2 + x[9]) + (1 + i[3]^2 + i[3])*(1 + x[10]^2 + x[10]) + (1 + i[3]^2 + i[3])*(1 + x[14]^2 + x[14]) + (1 + i[4]^2 + i[4])^2 + (1 + i[4]^2 + i[4])*(1 + x[7]^2 + x[7]) + (1 + i[4]^2 + i[4])*(1 + x[11]^2 + x[11]) + (1 + i[4]^2 + i[4])*(1 + x[15]^2 + x[15]) + (1 + i[5]^2 + i[5])^2 + (1 + i[5]^2 + i[5])*(1 + x[6]^2 + x[6]) + (1 + i[5]^2 + i[5])*(1 + x[10]^2 + x[10]) + (1 + i[5]^2 + i[5])*(1 + x[12]^2 + x[12]) + (1 + i[5]^2 + i[5])*(1 + x[16]^2 + x[16]) + (1 + x[6]^2 + x[6])^2 + (1 + x[6]^2 + x[6])*(1 + x[8]^2 + x[8]) + (1 + x[6]^2 + x[6])*(1 + x[15]^2 + x[15]) + (1 + x[7]^2 + x[7])^2 + (1 + x[7]^2 + x[7])*(1 + x[11]^2 + x[11]) + (1 + x[7]^2 + x[7])*(1 + x[13]^2 + x[13]) + (1 + x[8]^2 + x[8])^2 + (1 + x[8]^2 + x[8])*(1 + x[10]^2 + x[10]) + (1 + x[8]^2 + x[8])*(1 + x[15]^2 + x[15]) + (1 + x[9]^2 + x[9])^2 + (1 + x[9]^2 + x[9])*(1 + x[12]^2 + x[12]) + (1 + x[9]^2 + x[9])*(1 + x[16]^2 + x[16]) + (1 + x[10]^2 + x[10])^2 + (1 + x[10]^2 + x[10])*(1 + x[14]^2 + x[14]) + (1 + x[11]^2 + x[11])^2 + (1 + x[11]^2 + x[11])*(1 + x[13]^2 + x[13]) + (1 + x[12]^2 + x[12])^2 + (1 + x[12]^2 + x[12])*(1 + x[14]^2 + x[14]) + (1 + x[13]^2 + x[13])^2 + (1 + x[13]^2 + x[13])*(1 + x[14]^2 + x[14]) + sqr(1 + x[14]^2 + x[14]) + sqr(1 + x[15]^2 + x[15]) + (1 + x[16]^2 + x[16])^2) + objvar

@NLconstraint(m, c1, e1>=2.5)
@NLconstraint(m, c2, e2>=1.1)
@NLconstraint(m, c3, e3>=-3.1)
@NLconstraint(m, c4, e4>=-3.5)
@NLconstraint(m, c5, e5>=1.3)
@NLconstraint(m, c6, e6>=2.1)
@NLconstraint(m, c7, e7>=2.3)
@NLconstraint(m, c8, e8>=-1.5)
@NLconstraint(m, c9, e9==0)

@objective(m, Min, objvar)

labels = ["Solver","i[1]","i[2]","i[3]","i[4]","i[5]","x[6]","x[7]","x[8]","x[9]","x[10]","x[11]","x[12]","x[13]","x[14]","x[15]","x[16]","objvar","time (s)"]
results = labels
for solver = 1:4
    local opt,name = def_opt(solver)
    local res = []
    set_optimizer(m, opt)
    if solver == 3
        set_time_limit_sec(m, 600.0)
    end
    local t = @elapsed begin
        optimize!(m)
    end
    for k = 1:17
        if k<=5
            push!(res,value(m[:i][k]))
        elseif k<=16
            push!(res,value(m[:x][k]))
        else
            push!(res,value(m[:objvar]))
        end
    end
    push!(res,t)
    local data = vcat([name],res)
    global results = hcat(results,data)
end
df = DataFrames.DataFrame(results, :auto)
XLSX.writetable("data_test_MINLPLib.xlsx", df)
