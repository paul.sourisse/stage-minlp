using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP

solver = 2

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
end

model = Model(optimizer)

if solver == 3
    set_time_limit_sec(model, 600.0)
end

#Source: https://www.minlplib.org/jit1.html

@variable(model, 1 <= i1, Int)
@variable(model, 1 <= i2, Int)
@variable(model, 1 <= i3, Int)
@variable(model, 1 <= i4, Int)
@variable(model, 0.000252525252525253 <= x5)
@variable(model, 0.000508388408744281 <= x6)
@variable(model, 0.000635162601626016 <= x7)
@variable(model, 0.000636456211812627 <= x8)
@variable(model, 0.000861450107681263 <= x9)
@variable(model, 0.000438212094653812 <= x10)
@variable(model, 0.000433776749566223 <= x11)
@variable(model, 0.000289184499710815 <= x12)
@variable(model, 0.000224466891133558 <= x13)
@variable(model, 0.00033892560582952 <= x14)
@variable(model, 0.000224014336917563 <= x15)
@variable(model, 0.000337381916329285 <= x16)
@variable(model, x17)
@variable(model, x18)
@variable(model, x19)
@variable(model, x20)
@variable(model, x21)
@variable(model, x22)
@variable(model, x23)
@variable(model, x24)
@variable(model, x25)

obj = -(-(7.5/x5 + 5.625/x6 + 11.25/x7 + 7.5/x8 + 8.57142857142857/x9 + 7.14285714285714/x10 + 2.85714285714286/x11 + 5.71428571428571/x12 + 8.88888888888889/x13 + 8.88888888888889/x14 + 8.88888888888889/x15 + 4.44444444444444/x16) - 5000*i1 - 5500*i2 - 4000*i3 - 6000*i4 - 6000000*x17 - 9000000*x18 - 6000000*x19 - 9000000*x20 - 8000000*x21 - 8000000*x22 - 8000000*x23 - 10000000*x24 - 8000000*x25)
e2 = - 0.000252525252525253*i1 + x5
e3 = - 0.000508388408744281*i2 + x6
e4 = - 0.000635162601626016*i3 + x7
e5 = - 0.000636456211812627*i4 + x8
e6 = - 0.000861450107681263*i1 + x9
e7 = - 0.000438212094653812*i2 + x10
e8 = - 0.000433776749566223*i3 + x11
e9 = - 0.000289184499710815*i4 + x12
e10 = - 0.000224466891133558*i1 + x13
e11 = - 0.00033892560582952*i2 + x14
e12 = - 0.000224014336917563*i3 + x15
e13 = - 0.000337381916329285*i4 + x16
e14 = 5000*i1 + 5500*i2 + 4000*i3 + 6000*i4
e15 = 60*i1 + 50*i2 + 80*i3 + 40*i4
e16 = - x5 + x6 + x17
e17 = - x6 + x7 + x18
e18 = - x7 + x8 + x19
e19 = - x9 + x10 + x20
e20 = - x10 + x11 + x21
e21 = - x11 + x12 + x22
e22 = - x13 + x14 + x23
e23 = - x14 + x15 + x24
e24 = - x15 + x16 + x25
e25 = x5 - x6 + x17
e26 = x6 - x7 + x18
e27 = x7 - x8 + x19
e28 = x9 - x10 + x20
e29 = x10 - x11 + x21
e30 = x11 - x12 + x22
e31 = x13 - x14 + x23
e32 = x14 - x15 + x24
e33 =  x15 - x16 + x25

@NLconstraint(model, c2, e2 == 0)
@NLconstraint(model, c3, e3 == 0)
@NLconstraint(model, c4, e4 == 0)
@NLconstraint(model, c5, e5 == 0)
@NLconstraint(model, c6, e6 == 0)
@NLconstraint(model, c7, e7 == 0)
@NLconstraint(model, c8, e8 == 0)
@NLconstraint(model, c9, e9 == 0)
@NLconstraint(model, c10, e10 == 0)
@NLconstraint(model, c11, e11 == 0)
@NLconstraint(model, c12, e12 == 0)
@NLconstraint(model, c13, e13 == 0)
@NLconstraint(model, c14, e14 <= 6000000)
@NLconstraint(model, c15, e15 <= 3000)
@NLconstraint(model, c16, e16 >= 0)
@NLconstraint(model, c17, e17 >= 0)
@NLconstraint(model, c18, e18 >= 0)
@NLconstraint(model, c19, e19 >= 0)
@NLconstraint(model, c20, e20 >= 0)
@NLconstraint(model, c21, e21 >= 0)
@NLconstraint(model, c22, e22 >= 0)
@NLconstraint(model, c23, e23 >= 0)
@NLconstraint(model, c24, e24 >= 0)
@NLconstraint(model, c25, e25 >= 0)
@NLconstraint(model, c26, e26 >= 0)
@NLconstraint(model, c27, e27 >= 0)
@NLconstraint(model, c28, e28 >= 0)
@NLconstraint(model, c29, e29 >= 0)
@NLconstraint(model, c30, e30 >= 0)
@NLconstraint(model, c31, e31 >= 0)
@NLconstraint(model, c32, e32 >= 0)
@NLconstraint(model, c33, e33 >= 0)

@variable(model, z)
@objective(model, Min, z)
@NLconstraint(model, c0, z>= obj)

@time optimize!(model)