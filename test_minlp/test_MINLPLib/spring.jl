using MINLPLib
using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
ENV["ARTELYS_LICENSE"]="/Users/pol/Documents/Travail/CentraleSupelec/Stages/Stage_IETR/Code/com_solvers/artelys_licenses"
using KNITRO
import DataFrames, XLSX

solver = 1
auto = 0

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
elseif solver == 4
    optimizer = optimizer_with_attributes(KNITRO.Optimizer)
end

function sqr(x)
    return x^2
end

function def_opt(solvint::Int)
    if solvint == 1                                                                                     
        nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
        optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
        name = "Juniper"
    elseif solvint == 2
        optimizer = optimizer_with_attributes(KNITRO.Optimizer)
        name = "KNITRO"
    elseif solvint == 3                                                                                           
        optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)     
        name = "SCIP"                          
    elseif solvint == 4                                                                                            
        optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
        name = "Couenne"
    end
    return optimizer,name
end


m = Model()

@variable(m,x[1:5])
@variable(m,i[6],Int)
@variable(m,b[7:17],Bin)
@variable(m,objvar)

e1 = -(1.570796327 + 0.7853981635*i[6])*x[1]*sqr(x[2]) + objvar
e2 = -x[1]/x[2] + x[4]
e3 = -((-1 + 4*x[4])/(-4 + 4*x[4]) + 0.615/x[4]) + x[5]
e4 = 2546.47908913782*x[5]*x[4]/sqr(x[2])
e5 = -6.95652173913044e-7*x[4]^3*i[6]/x[2] + x[3]
e6 = (2.1 + 1.05*i[6])*x[2] + 1000*x[3]
e7 = x[1] + x[2]
e8 = x[2] - 0.207*b[7] - 0.225*b[8] - 0.244*b[9] - 0.263*b[10] - 0.283*b[11] - 0.307*b[12] - 0.331*b[13] - 0.362*b[14] - 0.394*b[15] - 0.4375*b[16] - 0.5*b[17]
e9 = b[7] + b[8] + b[9] + b[10] + b[11] + b[12] + b[13] + b[14] + b[15] + b[16] + b[17]


@NLconstraint(m, c1, e1==0)
@NLconstraint(m, c2, e2==0)
@NLconstraint(m, c3, e3==0)
@NLconstraint(m, c4, e4<=189000)
@NLconstraint(m, c5, e5==0)
@NLconstraint(m, c6, e6<=14)
@NLconstraint(m, c7, e7<=3)
@NLconstraint(m, c8, e8==0)
@NLconstraint(m, c9, e9==1)

@NLconstraint(m,b1,x[1]>=0.414)
@NLconstraint(m,b2,x[2]>=0.207)
@NLconstraint(m,b3,x[3]>=0.00178571428571429)
@NLconstraint(m,b4,x[3]<=0.02)
@NLconstraint(m,b5,x[4]>=1.1)
@NLconstraint(m,b6,i[6]>=1)

@objective(m, Min, objvar)

labels = ["Solver","x[1]","x[2]","x[3]","x[4]","x[5]","i[6]","b[7]","b[8]","b[9]","b[10]","b[11]","b[12]","b[13]","b[14]","b[15]","b[16]","b[17]","objvar","time (s)"]
results = labels
for solver = 1:4
    local opt,name = def_opt(solver)
    local res = []
    set_optimizer(m, opt)
    if solver == 3
        set_time_limit_sec(m, 600.0)
    end
    local t = @elapsed begin
        optimize!(m)
    end
    for k = 1:18
        if k<=5
            push!(res,value(m[:x][k]))
        elseif k==6
            push!(res,value(m[:i][k]))
        elseif k<=17
            push!(res,value(m[:b][k]))
        else
            push!(res,value(m[:objvar]))
        end
    end
    push!(res,t)
    local data = vcat([name],res)
    global results = hcat(results,data)
end
df = DataFrames.DataFrame(results, :auto)
XLSX.writetable("data_test_windfac.xlsx", df)
