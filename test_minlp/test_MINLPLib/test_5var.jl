using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP

solver = 2

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
end

model = Model(optimizer)

if solver == 3
    set_time_limit_sec(model, 600.0)
end

#Source: https://www.minlplib.org/nvs20.html

@variable(model, 1 <= i1 <= 200, Int)
@variable(model, 1 <= i2 <= 200, Int)
@variable(model, 1 <= i3 <= 200, Int)
@variable(model, 1 <= i4 <= 200, Int)
@variable(model, 1 <= i5 <= 200, Int)
@variable(model, 1 <= x6 <= 200)
@variable(model, 1 <= x7 <= 200)
@variable(model, 1 <= x8 <= 200)
@variable(model, 1 <= x9 <= 200)
@variable(model, 1 <= x10 <= 200)
@variable(model, 1 <= x11 <= 200)
@variable(model, 1 <= x12 <= 200)
@variable(model, 1 <= x13 <= 200)
@variable(model, 1 <= x14 <= 200)
@variable(model, 1 <= x15 <= 200)
@variable(model, 1 <= x16 <= 200)

function sqr(x)
    return x^2
end

obj = sqr(1 + sqr(i1) + i1) + (1 + sqr(i1) + i1)*(1 + sqr(i4) + i4) + (1 + sqr(i1) + i1)*(1 + sqr(x7) + x7) + (1 + sqr(i1) + i1)*(1 + sqr(x8) + x8) + (1 + sqr(i1) + i1)*(1 + sqr(x16) + x16) + sqr(1 + sqr(i2) + i2) + (1 + sqr(i2) + i2)*(1 + sqr(i3) + i3) + (1 + sqr(i2) + i2)*(1 + sqr(x7) + x7) + (1 + sqr(i2) + i2)*(1 + sqr(x10) + x10) + sqr(1 + sqr(i3) + i3) + (1 + sqr(i3) + i3)*(1 + sqr(x7) + x7) + (1 + sqr(i3) + i3)*(1 + sqr(x9) + x9) + (1 + sqr(i3) + i3)*(1 + sqr(x10) + x10) + (1 + sqr(i3) + i3)*(1 + sqr(x14) + x14) + sqr(1 + sqr(i4) + i4) + (1 + sqr(i4) + i4)*(1 + sqr(x7) + x7) + (1 + sqr(i4) + i4)*(1 + sqr(x11) + x11) + (1 + sqr(i4) + i4)*(1 + sqr(x15) + x15) + sqr(1 + sqr(i5) + i5) + (1 + sqr(i5) + i5)*(1 + sqr(x6) + x6) + (1 + sqr(i5) + i5)*(1 + sqr(x10) + x10) + (1 + sqr(i5) + i5)*(1 + sqr(x12) + x12) + (1 + sqr(i5) + i5)*(1 + sqr(x16) + x16) + sqr(1 + sqr(x6) + x6) + (1 + sqr(x6) + x6)*(1 + sqr(x8) + x8) + (1 + sqr(x6) + x6)*(1 + sqr(x15) + x15) + sqr(1 + sqr(x7) + x7) + (1 + sqr(x7) + x7)*(1 + sqr(x11) + x11) + (1 + sqr(x7) + x7)*(1 + sqr(x13) + x13) + sqr(1 + sqr(x8) + x8) + (1 + sqr(x8) + x8)*(1 + sqr(x10) + x10) + (1 + sqr(x8) + x8)*(1 + sqr(x15) + x15) + sqr(1 + sqr(x9) + x9) + (1 + sqr(x9) + x9)*(1 + sqr(x12) + x12) + (1 + sqr(x9) + x9)*(1 + sqr(x16) + x16) + sqr(1 + sqr(x10) + x10) + (1 + sqr(x10) + x10)*(1 + sqr(x14) + x14) + sqr(1 + sqr(x11) + x11) + (1 + sqr(x11) + x11)*(1 + sqr(x13) + x13) + sqr(1 + sqr(x12) + x12) + (1 + sqr(x12) + x12)*(1 + sqr(x14) + x14) + sqr(1 + sqr(x13) + x13) + (1 + sqr(x13) + x13)*(1 + sqr(x14) + x14) + sqr(1 + sqr(x14) + x14) + sqr(1 + sqr(x15) + x15) + sqr(1 + sqr(x16) + x16)
e1 = 0.22*i1 + 0.2*i2 + 0.19*i3 + 0.25*i4 + 0.15*i5 + 0.11*x6 + 0.12*x7 + 0.13*x8 + x9
e2 = - 1.46*i1 - 1.3*i3 + 1.82*i4 - 1.15*i5 + 0.8*x7 + x10
e3 = 1.29*i1 - 0.89*i2 - 1.16*i5 - 0.96*x6 - 0.49*x8 + x11
e4 = - 1.1*i1 - 1.06*i2 + 0.95*i3 - 0.54*i4 - 1.78*x6 - 0.41*x7 + x12
e5 = - 1.43*i4 + 1.51*i5 + 0.59*x6 - 0.33*x7 - 0.43*x8 + x13
e6 = - 1.72*i2 - 0.33*i3 + 1.62*i5 + 1.24*x6 + 0.21*x7 - 0.26*x8 + x14
e7 = 1.12*i1 + 0.31*i4 + 1.12*x7 - 0.36*x9 + x15
e8 = 0.45*i2 + 0.26*i3 - 1.1*i4 + 0.58*i5 - 1.03*x7 + 0.1*x8 + x16

@NLconstraint(model, c1, e1 >= 2.5)
@NLconstraint(model, c2, e2 >= 1.1)
@NLconstraint(model, c3, e3 >= -3.1)
@NLconstraint(model, c4, e4 >= -3.5)
@NLconstraint(model, c5, e5 >= 1.3)
@NLconstraint(model, c6, e6 >= 2.1)
@NLconstraint(model, c7, e7 >= 2.3)
@NLconstraint(model, c8, e8 >= -1.5)

@variable(model, z)
@objective(model, Min, z)
@NLconstraint(model, c0, z>= obj)

@time optimize!(model)