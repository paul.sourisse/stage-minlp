using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP

solver = 1

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
end

model = Model(optimizer)

if solver == 3
    set_time_limit_sec(model, 600.0)
end

#Source: https://www.minlplib.org/nvs05.html

@variable(model, 1 <= i1 <= 200, Int)
@variable(model, 1 <= i2 <= 200, Int)
@variable(model, 0.01 <= x3 <= 200)
@variable(model, 0.01 <= x4 <= 200)
@variable(model, x5)
@variable(model, x6)
@variable(model, x7)
@variable(model, x8)
@variable(model, objvar)

e1 = -4243.28147100424/(x3*x4) + x5 
e2 = -sqrt(0.25*x4^2 + (0.5*i1 + 0.5*x3)^2) + x7
e3 = -(59405.9405940594 + 2121.64073550212*x4)*x7/(x3*x4*(0.0833333333333333*x4^2 + (0.5*i1 + 0.5*x3)^2)) + x6
e4 = -0.5*x4/x7 + x8
e5 = -sqrt(x5^2 + 2*x5*x6*x8 + x6^2)
e6 = -504000/(i1^2*i2)
e7 = i2 - x3
e8 = 0.0204744897959184*sqrt(10^15*i2^3*i1*i1*i2^3)*(1 - 0.0282346219657891*i1)
e9 = -0.21952/(i1^3*i2)
e10 = -(1.10471*x3^2*x4 + 0.04811*i1*i2*(14 + x4)) + objvar

@NLconstraint(model, c1, e1 == 0)
@NLconstraint(model, c2, e2 == 0)
@NLconstraint(model, c3, e3 == 0)
@NLconstraint(model, c4, e4 == 0)
@NLconstraint(model, c5, e5 >= -13600)
@NLconstraint(model, c6, e6 >= -30000)
@NLconstraint(model, c7, e7 >= 0)
@NLconstraint(model, c8, e8 >= 6000)
@NLconstraint(model, c9, e9 >= -0.25)
@NLconstraint(model, c10, e10 == 0)

@objective(model, Min, objvar)

@time optimize!(model)