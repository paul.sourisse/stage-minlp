using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP

solver = 2

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
end

model = Model(optimizer)

if solver == 3
    set_time_limit_sec(model, 600.0)
end

#Source: https://www.minlplib.org/windfac.html

@variable(model, 1 <= i1 <= 10, Int)
@variable(model, 1 <= i2 <= 100, Int)
@variable(model, x3)
@variable(model, x4)
@variable(model, 1 <= i5 <= 100, Int)
@variable(model, x6)
@variable(model, x7)
@variable(model, x8)
@variable(model, x9)
@variable(model, x10)
@variable(model, x11)
@variable(model, x12)
@variable(model, x13)
@variable(model, x14)
@variable(model, 0.8 <= x15)

e1 = - 12*i1 + i2
e2 = -12.566370616/i2 + x3
e3 = - 0.25*i2 + x4
e4 = - x4 + i5
e5 = sin(0.5*x3)*i1*x6 - sin(0.5*i1*x3)
e6 = -sin(1.570796327*i5/x4) + x9
e7 = -x9*x6 + x15
e8 = sin(1.5*x3)*i1*x7 - sin(1.5*i1*x3)
e9 = -sin(4.712388981*i5/x4) + x10
e10 = -x10*x7 + x13
e11 = sin(2.5*x3)*i1*x8 - sin(2.5*i1*x3)
e12 = -sin(7.853981635*i5/x4) + x11
e13 = -x11*x8 + x14
obj = x13*x13 + x14*x14

@NLconstraint(model, c1, e1 == 0)
@NLconstraint(model, c2, e2 == 0)
@NLconstraint(model, c3, e3 == 0)
@NLconstraint(model, c4, e4 == -1)
@NLconstraint(model, c5, e5 == 0)
@NLconstraint(model, c6, e6 == 0)
@NLconstraint(model, c7, e7 == 0)
@NLconstraint(model, c8, e8 == 0)
@NLconstraint(model, c9, e9 == 0)
@NLconstraint(model, c10, e10 == 0)
@NLconstraint(model, c11, e11 == 0)
@NLconstraint(model, c12, e12 == 0)
@NLconstraint(model, c13, e13 == 0)

@variable(model, z)
@objective(model, Min, z)
@NLconstraint(model, c0, z>= obj)

@time optimize!(model)