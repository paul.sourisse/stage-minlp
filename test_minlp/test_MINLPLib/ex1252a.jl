using MINLPLib
using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
ENV["ARTELYS_LICENSE"]="/Users/pol/Documents/Travail/CentraleSupelec/Stages/Stage_IETR/Code/com_solvers/artelys_licenses"
using KNITRO

m = fetch_model("minlp2/ex1252a")
#println(m)

solver = 3

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
elseif solver == 4
    optimizer = optimizer_with_attributes(KNITRO.Optimizer,"mip_multistart" => 1)
end
if solver == 3
    set_time_limit_sec(m, 600.0)
end

set_optimizer(m,optimizer)
@time optimize!(m)

