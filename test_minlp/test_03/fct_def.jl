using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
using NEOSServer

#Constants:
kr = 0.7
q = 3
m = 1
lcu = 386
lAl = 204
he = 12
ha = 4
pcu = 0.018e-6
TJ = 75
Te = 25
Tc = 20
beta = 0.85
g = 1e-3

function actuator_def!(optimizer,obj::Int)
    model = Model(optimizer)
    
    #Variables
    #Continuous variables:
    @variable(model, 60e-3<=D<=200e-3)
    @variable(model, 3e-3<=la<=50e-3)
    @variable(model, 3e-3<=E<=50e-3)
    @variable(model, 3e-3<=C<=50e-3)
    @variable(model, 20e-3<= L<= 200e-3)
    @variable(model, 4e-3<=d<=50e-3)
    @variable(model, 2e-3<=a<=50e-3)
    @variable(model, 3e6<= J<=6e6)
    #Integer variables:
    @variable(model, 1<=p<=6, Int)
    #Binary variables:
    @variable(model, srb, Bin)
    sr = 2 * srb - 1
    @variable(model, se, Bin)
    @variable(model, sfb, Bin)
    sf = 2 * sfb - 1
    @variable(model, sm, Bin)
    @variable(model, smt, Bin)

    #Expressions:
    l = D/L
    t = d / (d + a)
    #Magnetic material model:
    M = 0.6 + 0.3 * sm
    BM = 1.2 + 0.3 * smt
    lmc = 80 - 10 * smt
    #Magnetic model:
    Kf = 1.5 * p * (E + g) * (beta / D) * (1 - se) * ((1 + sf) / 2)
    KT = (pi/2) * (((1 + sf) / 2) * (1 - Kf) * sqrt(beta) + ((1 - sf) / 2) * (sqrt(2) / 2))
    A = kr * E * J * (a / (se * d + a))
    Be = (2 * M * la) / (sr * D * log((D + 2 * E * sr * (1 - se))/(D - 2 * sr * (la + g))))
    Tem = D * L * (D + sr * (1 - se) * E) * KT * Be * A
    Bc = (D / (2 * p * C)) * (((1+ sf) / 2) * beta * (pi / 2) + ((1 - sf) / 2)) * Be
    Bt = ((d + a) / d) * Be
    Ne = (pi * (D + sr * E)) / (d + a)
    Nec = 2 * p * q * m
    CstM = D / 2 - C - ((1 - sr) / 2) * E - ((1 + sr) / 2) * (g + la)
    #Thermal model:
    ae = ((2 * a) / (D + sr * E)) * se + beta * (pi/p) * (1 - se)
    RJ = kr * (sr / (lcu * ae * L)) * log(1 + sr * ((2 * E) / D))
    ad = ((2 *  d) / (D + sr * E)) * se + (1 - beta) * (pi/p) * (1 - se)
    Rd = (sr / (lmc * (ad/2) * L)) * log(1 + sr * (2 * E / D))
    ac = 2 * ((a + d) / (D + sr * E)) * se + (pi/p) * (1 - se)
    Rc = (sr / (lmc * ac * L)) * log(1 + sr * ((2 * C) / (D + sr * 2 * E)))
    Re = 2 / (D * ae * L * he)
    Reps = 4 / (D * ad * L * he)
    Rg = 2 / ((D + 2 * sr * (E + C)) * ac * L * ha)
    PJ = pcu * pi * J * L * (D + sr * (1 - se) * E) * A
    PTh = PJ / (2 * p * (1 + (q * m - 1) * se))
    Req1 = (Re * (Rd + Reps)) / (2 * Re + Rd + Reps)
    Req2 = Rc + Rg
    PThc = (((TJ - Te) / Req1) + ((TJ - Tc)/ Req2))* (1 / (1 + RJ * ((1/Req1) + (1/Req2))))
    #Geometric model:
    Vm = beta* pi * L * la * (D  -sr * (2 * g + la))
    Vg = (pi * L / 4) * (((1+ sr) / 2) * (D + 2 * (E + C))^2 + ((1 - sr)/2) * (D + 2 * (g + la + C))^2)

    #constraints:
    @NLconstraint(model, CNe, Ne == Nec)
    @NLconstraint(model, CBc, Bc <= BM)
    @NLconstraint(model, CBt, Bt <= BM)
    @NLconstraint(model, CM, CstM >= 0)
    @NLconstraint(model, CPTh, PTh == PThc)
    @NLconstraint(model, Cl, l == 2)
    @NLconstraint(model, Ct, t == 0.6)
    @NLconstraint(model, CTem, Tem == 10)

    #objective:
    @variable(model, z)
    @objective(model, Min, z)
    if obj==1
        @NLconstraint(model, z >= Vm)
    elseif obj==2
        @NLconstraint(model, z >= Vg)
    end

    return model
end

function get_results(model::JuMP.Model,opti::Int)
    names = []
    vals = []
    av = JuMP.all_variables(model)
    for i = 1:length(av)
        varn = name(av[i])
        push!(names,varn)
        if opti==1
            varv = value(av[i])
            push!(vals,varv)
        end
    end
    return names,vals
end

function test_values(value,opti::Int)
    names = ["A","Be","Bc","Bt","Tem","Ne","Nec","PTh","PThc","l","t","Vm","Vg"]
    if opti==0
        return names,[]
    end

    D = value[1]
    la = value[2]
    E = value[3]
    C = value[4]
    L = value[5]
    d = value[6]
    a = value[7]
    J = value[8]
    p = value[9]
    srb = value[10]
    sr = 2 * srb - 1
    se = value[11]
    sfb = value[12]
    sf = 2 * sfb - 1
    sm = value[13]
    smt = value[14]
    
    l = D/L
    t = d / (d + a)
    M = 0.6 + 0.3 * sm
    #BM = 1.2 + 0.3 * smt
    lmc = 80 - 10 * smt

    #Magnetic model:
    Kf = 1.5 * p * (E + g) * (beta / D) * (1 - se) * ((1 + sf) / 2)
    KT = (pi/2) * (((1 + sf) / 2) * (1 - Kf) * sqrt(beta) + ((1 - sf) / 2) * (sqrt(2) / 2))
    A = kr * E * J * (a / (se * d + a))
    Be = (2 * M * la) / (sr * D * log((D + 2 * E * sr * (1 - se))/(D - 2 * sr * (la + g))))
    Tem = D * L * (D + sr * (1 - se) * E) * KT * Be * A
    Bc = (D / (2 * p * C)) * (((1+ sf) / 2) * beta * (pi / 2) + ((1 - sf) / 2)) * Be
    Bt = ((d + a) / d) * Be
    Ne = (pi * (D + sr * E)) / (d + a)
    Nec = 2 * p * q * m
    #CstM = D / 2 - C - ((1 - sr) / 2) * E - ((1 + sr) / 2) * (g + la)

    #Thermal model:
    ae = ((2 * a) / (D + sr * E)) * se + beta * (pi/p) * (1 - se)
    RJ = kr * (sr / (lcu * ae * L)) * log(1 + sr * ((2 * E) / D))
    ad = ((2 *  d) / (D + sr * E)) * se + (1 - beta) * (pi/p) * (1 - se)
    Rd = (sr / (lmc * (ad/2) * L)) * log(1 + sr * (2 * E / D))
    ac = 2 * ((a + d) / (D + sr * E)) * se + (pi/p) * (1 - se)
    Rc = (sr / (lmc * ac * L)) * log(1 + sr * ((2 * C) / (D + sr * 2 * E)))
    Re = 2 / (D * ae * L * he)
    Reps = 4 / (D * ad * L * he)
    Rg = 2 / ((D + 2 * sr * (E + C)) * ac * L * ha)
    PJ = pcu * pi * J * L * (D + sr * (1 - se) * E) * A
    PTh = PJ / (2 * p * (1 + (q * m - 1) * se))
    Req1 = (Re * (Rd + Reps)) / (2 * Re + Rd + Reps)
    Req2 = Rc + Rg
    PThc = (((TJ - Te) / Req1) + ((TJ - Tc)/ Req2))* (1 / (1 + RJ * ((1/Req1) + (1/Req2))))

    #Geometric model:
    Vm = beta* pi * L * la * (D  -sr * (2 * g + la))
    Vg = (pi * L / 4) * (((1+ sr) / 2) * (D + 2 * (E + C))^2 + ((1 - sr)/2) * (D + 2 * (g + la + C))^2)

    
    values = [A,Be,Bc,Bt,Tem,Ne,Nec,PTh,PThc,l,t,Vm,Vg]
    return names,values
end

function def_optimizer(solver)
    if solver == 1                                                                                                  #Juniper + Ipopt
        nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
        optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
        name = "Juniper"
    elseif solver == 2                                                                                              #Couenne
        optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
        name = "Couenne"
    elseif solver == 3                                                                                              #SCIP
        optimizer = optimizer_with_attributes(SCIP.Optimizer,"display/verblevel" => 0, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
        name = "SCIP"
    elseif solver == 4
        optimizer = NEOSServer.Optimizer(email = "paul.sourisse@student-cs.fr", sover = "FICO-Xpress")
        name = "FICO-Xpress"
    end
    return optimizer,name
end

function valeurs_article(obj)
    if obj == 1                                #Vm
        D = 98.65e-3
        L = 49.33e-3
        la = 3.20e-3
        E = 25.5e-3
        C = 44.5e-3
        J = 3.01e6
        a = 26.0e-3
        d = 39e-3
        p = 1
        sr = 1
        se = 1
        sf = 1
        sm = 1
        smt = 0
    elseif obj == 2
        D = 102e-3
        L = 51e-3
        la = 8.2e-3
        E = 17e-3
        C = 7e-3
        J = 3.05e6
        a = 3e-3
        d = 4.50e-3
        p = 6
        sr = -1
        se = 1
        sf = 1
        sm = 1
        smt = 1 
    end
    sfb = (sf + 1)/2
    srb = (sr + 1)/2

    l = D/L
    t = d / (d + a)
    
    M = 0.6 + 0.3 * sm
    lmc = 80 - 10 * smt
    
    Kf = 1.5 * p * (E + g) * (beta / D) * (1 - se) * ((1 + sf) / 2)
    KT = (pi/2) * (((1 + sf) / 2) * (1 - Kf) * sqrt(beta) + ((1 - sf) / 2) * (sqrt(2) / 2))
    A = kr * E * J * (a / (se * d + a))
    Be = (2 * M * la) / (sr * D * log((D + 2 * E * sr * (1 - se))/(D - 2 * sr * (la + g))))
    Tem = D * L * (D + sr * (1 - se) * E) * KT * Be * A
    Bc = (D / (2 * p * C)) * (((1+ sf) / 2) * beta * (pi / 2) + ((1 - sf) / 2)) * Be
    Bt = ((d + a) / d) * Be
    Ne = (pi * (D + sr * E)) / (d + a)
    Nec = 2 * p * q * m

    ae = ((2 * a) / (D + sr * E)) * se + beta * (pi/p) * (1 - se)
    RJ = kr * (sr / (lcu * ae * L)) * log(1 + sr * ((2 * E) / D))
    ad = ((2 *  d) / (D + sr * E)) * se + (1 - beta) * (pi/p) * (1 - se)
    Rd = (sr / (lmc * (ad/2) * L)) * log(1 + sr * (2 * E / D))
    ac = 2 * ((a + d) / (D + sr * E)) * se + (pi/p) * (1 - se)
    Rc = (sr / (lmc * ac * L)) * log(1 + sr * ((2 * C) / (D + sr * 2 * E)))
    Re = 2 / (D * ae * L * he)
    Reps = 4 / (D * ad * L * he)
    Rg = 2 / ((D + 2 * sr * (E + C)) * ac * L * ha)
    PJ = pcu * pi * J * L * (D + sr * (1 - se) * E) * A
    PTh = PJ / (2 * p * (1 + (q * m - 1) * se))
    Req1 = (Re * (Rd + Reps)) / (2 * Re + Rd + Reps)
    Req2 = Rc + Rg
    PThc = (((TJ - Te) / Req1) + ((TJ - Tc)/ Req2))* (1 / (1 + RJ * ((1/Req1) + (1/Req2))))
    #Geometric model:
    Vm = beta* pi * L * la * (D  -sr * (2 * g + la))
    Vg = (pi * L / 4) * (((1+ sr) / 2) * (D + 2 * (E + C))^2 + ((1 - sr)/2) * (D + 2 * (g + la + C))^2)

    return [D,la,E,C,L,d,a,J,p,srb,se,sfb,sm,smt,Vm,A,Be,Bc,Bt,Tem,Ne,Nec,PTh,PThc,l,t,Vm,Vg,"-"]
end