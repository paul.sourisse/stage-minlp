import DataFrames, XLSX
include("fct_def_reform.jl")

#solver = 1
#obj = 1
objectives = ["Vm","Vg"]

model_init,solver_name = def_optimizer(1)
act = actuator_def!(model_init,1)

par_name = ["obj","solver"]
varname,varval = get_results(act,0)
fname,fval = test_values(varval,0)
rname = vcat(varname,fname)
results = vcat(par_name,rname)
push!(results, "time")
for obj = 1:2
    obj_name = objectives[obj]
    println(obj_name)
    art_par = [obj_name,"Article"]
    art_val = valeurs_article(obj)
    art = vcat(art_par,art_val)
    global results = hcat(results,art)
    for solver = 1:3
        model,solver_name = def_optimizer(solver)
        println(solver_name)
        act = actuator_def!(model,obj)
        par_val = [obj_name,solver_name]
        local t = @elapsed begin
            optimize!(act)
        end
        varname,varval = get_results(act,1)
        fname,fval = test_values(varval,1)
        rval = vcat(varval,fval)
        val = vcat(par_val,rval)
        push!(val,t)
        global results = hcat(results, val)
    end
end

df = DataFrames.DataFrame(results, :auto)

XLSX.writetable("data03ref.xlsx", df)