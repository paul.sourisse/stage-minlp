using JuMP
using Juniper, Ipopt
import AmplNLWriter, Couenne_jll
import AmplNLWriter, Bonmin_jll
using SCIP
using NEOSServer

solver = 3

if solver == 1                                                                                                  #Juniper + Ipopt
    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0)
    optimizer = optimizer_with_attributes(Juniper.Optimizer, "nl_solver"=>nl_solver)
elseif solver == 2                                                                                              #Couenne
    optimizer = () -> AmplNLWriter.Optimizer(Couenne_jll.amplexe)
elseif solver == 3                                                                                              #SCIP
    optimizer = optimizer_with_attributes(SCIP.Optimizer, "limits/gap" => 0.05)                                 #"display/verblevel" => 0,
elseif solver == 4
    optimizer = () -> NEOSServer.Optimizer(email = "paul.sourisse@student-cs.fr", solver = "FICO-Xpress")
end

model = Model(optimizer)
#set_time_limit_sec(model, 60.0)

#Constants:
kr = 0.7
q = 3
m = 1
lcu = 386
lAl = 204
he = 12
ha = 4
pcu = 0.018e-6
TJ = 75
Te = 25
Tc = 20
beta = 0.85
g = 1e-3
    
#Variables
#Continuous variables:
@variable(model, 60e-3<=D<=200e-3, start = 0.10073828219057532)
@variable(model, 3e-3<=la<=50e-3, start = 0.0030000751250545375)
@variable(model, 3e-3<=E<=50e-3, start = 0.02482351329086718)
@variable(model, 3e-3<=C<=50e-3, start = 0.04636078430459137)
@variable(model, 20e-3<= L<= 200e-3, start = 0.050369141095287666)
@variable(model, 4e-3<=d<=50e-3, start = 0.03944640142560438)
@variable(model, 2e-3<=a<=50e-3, start = 0.02629760095040293)
@variable(model, 3e6<= J<=6e6, start = 3.000084281376845e6)
#Integer variables:
@variable(model, 1<=p<=6, Int, start = 1)
#Binary variables:
@variable(model, srb, Bin, start = 1)
sr = 2 * srb - 1
@variable(model, se, Bin, start = 1)
@variable(model, sfb, Bin, start = 1)
sf = 2 * sfb - 1
@variable(model, sm, Bin, start = 1)
@variable(model, smt, Bin, start = 1)

#Expressions:
l = D/L
t = d / (d + a)
#Magnetic material model:
M = 0.6 + 0.3 * sm
BM = 1.2 + 0.3 * smt
lmc = 80 - 10 * smt
#Magnetic model:
Kf = 1.5 * p * (E + g) * (beta / D) * (1 - se) * ((1 + sf) / 2)
KT = (pi/2) * (((1 + sf) / 2) * (1 - Kf) * sqrt(beta) + ((1 - sf) / 2) * (sqrt(2) / 2))
A = kr * E * J * (a / (se * d + a))
Be = (2 * M * la) / (sr * D * log((D + 2 * E * sr * (1 - se))/(D - 2 * sr * (la + g))))
Tem = D * L * (D + sr * (1 - se) * E) * KT * Be * A
Bc = (D / (2 * p * C)) * (((1+ sf) / 2) * beta * (pi / 2) + ((1 - sf) / 2)) * Be
Bt = ((d + a) / d) * Be
Ne = (pi * (D + sr * E)) / (d + a)
Nec = 2 * p * q * m
CstM = D / 2 - C - ((1 - sr) / 2) * E - ((1 + sr) / 2) * (g + la)
#Thermal model:
ae = ((2 * a) / (D + sr * E)) * se + beta * (pi/p) * (1 - se)
RJ = kr * (sr / (lcu * ae * L)) * log(1 + sr * ((2 * E) / D))
ad = ((2 *  d) / (D + sr * E)) * se + (1 - beta) * (pi/p) * (1 - se)
Rd = (sr / (lmc * (ad/2) * L)) * log(1 + sr * (2 * E / D))
ac = 2 * ((a + d) / (D + sr * E)) * se + (pi/p) * (1 - se)
Rc = (sr / (lmc * ac * L)) * log(1 + sr * ((2 * C) / (D + sr * 2 * E)))
Re = 2 / (D * ae * L * he)
Reps = 4 / (D * ad * L * he)
Rg = 2 / ((D + 2 * sr * (E + C)) * ac * L * ha)
PJ = pcu * pi * J * L * (D + sr * (1 - se) * E) * A
PTh = PJ / (2 * p * (1 + (q * m - 1) * se))
Req1 = (Re * (Rd + Reps)) / (2 * Re + Rd + Reps)
Req2 = Rc + Rg
PThc = (((TJ - Te) / Req1) + ((TJ - Tc)/ Req2))* (1 / (1 + RJ * ((1/Req1) + (1/Req2))))
#Geometric model:
Vm = beta * pi * L * la * (D - sr * (2 * g + la))
Vg = (pi * L / 4) * (((1+ sr) / 2) * (D + 2 * (E + C))^2 + ((1 - sr)/2) * (D + 2 * (g + la + C))^2)

#constraints:
@NLconstraint(model, CNe, Ne == Nec)
#@NLconstraint(model, CA, 1*10^4<=A<=10*10^4)
@NLconstraint(model, CAl, 1*10^4<=A)
@NLconstraint(model, CAu, A<=10*10^4)
@NLconstraint(model, CBel, 0.1<= Be)
@NLconstraint(model, CBeu, Be <= 0.9)
@NLconstraint(model, CBcl, 0.9<= Bc)
@NLconstraint(model, CBcu, Bc <= BM)
@NLconstraint(model, CBtl, 0.9<= Bt)
@NLconstraint(model, CBtu, Bt <= BM)
@NLconstraint(model, CM, CstM >= 0)
@NLconstraint(model, CPTh, PTh == PThc)
@NLconstraint(model, Cl, l == 2)
@NLconstraint(model, Ct, t == 0.6)
@NLconstraint(model, CTem, Tem == 10)

#objective:
@variable(model, z)
@objective(model, Min, z)
@NLconstraint(model, z >= Vm)

@time optimize!(model)