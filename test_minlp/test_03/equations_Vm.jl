#Constants
kr = 0.7
q = 3
m = 1
lcu = 386
lAl = 204
he = 12
ha = 4
pcu = 0.018e-6
TJ = 75
Te = 25
Tc = 20

#l = 2
#t = 0.6
beta = 0.85
g = 1e-3

#Variables:
D = 98.65e-3
L = 49.33e-3

la = 3.20e-3
E = 25.5e-3
C = 44.5e-3

J = 3.01e6
a = 26.0e-3
d = 39e-3
p = 1
sr = 1
se = 1
sf = 1
sm = 1
smt = 0

l = D/L
t = d / (d + a)

M = 0.6 + 0.3 * sm
BM = 1.2 + 0.3 * smt
lmc = 80 - 10 * smt

#Magnetic model:
Kf = 1.5 * p * (E + g) * (beta / D) * (1 - se) * ((1 + sf) / 2)
KT = (pi/2) * (((1 + sf) / 2) * (1 - Kf) * sqrt(beta) + ((1 - sf) / 2) * (sqrt(2) / 2))
A = kr * E * J * (a / (se * d + a))
Be = (2 * M[sm] * la) / (sr * D * log((D + 2 * E * sr * (1 - se))/(D - 2 * sr * (la + g))))
Tem = D * L * (D + sr * (1 - se) * E) * KT * Be * A
Bc = (D / (2 * p * C)) * (((1+ sf) / 2) * beta * (pi / 2) + ((1 - sf) / 2)) * Be
Bt = ((d + a) / d) * Be
Ne = (pi * (D + sr * E)) / (d + a)
Nec = 2 * p * q * m
CstM = D / 2 - C - ((1 - sr) / 2) * E - ((1 + sr) / 2) * (g + la)

#Thermal model:
ae = ((2 * a) / (D + sr * E)) * se + beta * (pi/p) * (1 - se)
RJ = kr * (sr / (lcu * ae * L)) * log(1 + sr * ((2 * E) / D))
ad = ((2 *  d) / (D + sr * E)) * se + (1 - beta) * (pi/p) * (1 - se)
Rd = (sr / (lmc[smt] * (ad/2) * L)) * log(1 + sr * (2 * E / D))
ac = 2 * ((a + d) / (D + sr * E)) * se + (pi/p) * (1 - se)
Rc = (sr / (lmc[smt] * ac * L)) * log(1 + sr * ((2 * C) / (D + sr * 2 * E)))
Re = 2 / (D * ae * L * he)
Reps = 4 / (D * ad * L * he)
Rg = 2 / ((D + 2 * sr * (E + C)) * ac * L * ha)
PJ = pcu * pi * J * L * (D + sr * (1 - se) * E) * A
PTh = PJ / (2 * p * (1 + (q * m - 1) * se))
Req1 = (Re * (Rd + Reps)) / (2 * Re + Rd + Reps)
Req2 = Rc + Rg
PThc = (((TJ - Te) / Req1) + ((TJ - Tc)/ Req2))* (1 / (1 + RJ * ((1/Req1) + (1/Req2))))

#Geometric model:
Vm = beta* pi * L * la * (D  -sr * (2 * g + la))
Vg = (pi * L / 4) * (((1+ sr) / 2) * (D + 2 * (E + C))^2 + ((1 - sr)/2) * (D + 2 * (g + la + C))^2)

results_Vm = [D,la,E,E,C,L,d,a,J,p,sr,se,sf,sm,smt,Vm,A,Be,Bc,Bt,Tem,Ne,Nec,PTh,PThc,l,t,Vm,Vg,"-"]