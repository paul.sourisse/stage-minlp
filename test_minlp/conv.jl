using JuMP

function ftest(b::Int,N::Int)
    m = Model()
    if b==0
        @variable(m, 0<= x[1:N]<=1, Int)
    else
        @variable(m, x[1:N], Bin)
    end
    return m
end

N = 10^9

t1 = @elapsed begin
    m = ftest(0,N)
end

t2 = @elapsed begin
    m = ftest(1,N)
end