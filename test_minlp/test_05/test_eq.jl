x1 = 138.5e-3                           #D, mm
x2 = 54.2e-3                            #L, mm
x3 = 1e-3               #fixed          #g, mm
x4 = 4.2e-3                             #la, mm
x5 = 6e-3                               #E, mm 
x6 = 4.6e-3                             #C, mm 
x7 = 0.85               #fixed          #beta, -
x8 = 5.7e6                              #j, A/mm^2
x9 = 4.2e-3                             #a, mm 
x10 = 4.2e-3                            #d, mm 
x11 = 130               #fixed          #TJ, °C
x12 = 55                #fixed          #Te, °C 
x13 = 50                #fixed          #Tc, °C

z1 = 9                                  #p, -
z2 = 3                  #fixed          #q, -
z3 = 1                  #fixed          #m, -

b1 = 1                                  #sr, -
b2 = 1                                  #se, -
b3 = 1                                  #sf, -

k1 = "mo"                               #sm, -
k2 = "po"                               #smt, -

kr = 0.7
pcu = 0.018e-6                          #Ohm.m 
dAl = 2700                              #kg/m^3
dco = 8900                              #kg/m^3
he = 12
ha = 4

function M(k::String)                   #T
    if k == "pl"
        return 0.6
    end
    if k == "mo"
        return 0.9
    end
end

function dPM(k::String)                    #kg/m^3
    if k == "pl"
        return 6000
    end
    if k == "mo"
        return 7900
    end
end

function BM(k::String)                  #T
    if k == "po"
        return 1.2
    end
    if k == "st"
        return 1.5
    end
end

function dCM(k::String)                    #kg/m^3
    if k == "po"
        return 6000
    end
    if k == "st"
        return 7900
    end
end

kd = b2 * (x9 / (x9 + x10))
KS = kr * x5 * x8 * (b2 * kd + (1 - b2))
Kf = (1 - b2) * b3 * (1.5 * z1 * x7 *((x5 + x3) / x1))
kG = (pi/2) * (b3 * (1 - Kf) * sqrt(x7) + (1 - b3) * (sqrt(2)/2) * sin(x7 * (pi/2)))
Ne = b2 * pi * ((x1 + (2 * b1 - 1) * x5) / (x9 + x10))
Nec = b2 * 2 * z1 * z2 * z3
kc = 1 / (1 - b2 * ((Ne * x9^2) / (5 * pi * x1 * x3 + pi * x1 * x9)))
Be = (2 * M(k1) * x4) / ((2 * b1 - 1) * kc * x1 * log((x1 + 2 * (2 * b1 - 1) * (1 - b2) * x5) / (x1 - 2 * (2 * b1 - 1) * (x3 + x4))))
Gem = x1 * x2 * (x1 + (1 - b2) * (2 * b1 -1) * x5) * kG * Be * 12.3e3
Bt = ((x9 + x10) / x10) * Be
Bc = (x1 / (2 * z1 * x6)) * (((pi * x7) / 2) * b3 + (1 - b3)) * Be
BMu = BM(k2)

PTh = pcu * pi * x8 * x2 * (x1 + (2 * b1 - 1) * (1 - b2) * x5) * KS
PThc = pi * x2 * (ha * (x1 + 2 * (2 * b1 - 1) * (x5 + x6)) * (x11 - x12) + x1 * he * (x11 - x13))

Ri = x1 / 2 - x6 - (b1 - 1) * x5 - b1 * (x3 + x4)
Re = x1 / 2 + x6 + b1 * x5 + (b1 - 1) * (x3 + x4)
cRu = 80e-3
cRl = 50e-3

Vm = pi * x7 * x2 * x4 * (x1 - (2 * b1 - 1) * (2 * x3 + x4))
Vc = 2 * pi * x2 * x6 * (x1 + (2 * b1 - 1) * (x5 - x3 - x4))
Vd = pi * x2 * x5 * (x1 + (2 * b1 - 1) * x5) * ((1 - b2) * (1 - x7) + b2 * (x10 / (x9 + x10)))
Vco = kr * pi * x2 * x5 * (x1 + (2 * b1 - 1) * x5) * ((1 - b2) * x7 + b2 * (x9 / (x9 + x10)))
Vg = (pi * x2 / 4) * (b1 * (x1 + 2 * (x5 + x6))^2 + (1 - b1) * (x1 + 2 * (x3 + x4 + x6))^2)

Ma = dPM(k1) * Vm + dCM(k2) * Vc + (dCM(k2) * b2 + dAl * (1 - b2)) * Vd + dco * Vco